#include "Sensors.h"

/******************  LightSensor **************************/
void LightSensor::_update(long unsigned now){
  _sayMyName();
  _specialLightSensorFunction();
}
void LightSensor::_specialLightSensorFunction(){
  Serial.println("I'm doin' special light sensing stuff!    *** The sun is in my eyes!\n");
}

LightSensor::LightSensor(String name, long unsigned updatePeriod): Updateable(name,updatePeriod){}


/******************  AcousticSensor **************************/
void AcousticSensor::_update(long unsigned now){
  _sayMyName();
  _specialAcousticSensorFunction();
}

void AcousticSensor::_specialAcousticSensorFunction(){
  Serial.println("I'm doin' special acoustic sensing stuff! *** What did you say, I can't hear you!\n");
}

AcousticSensor::AcousticSensor(String name, long unsigned updatePeriod): Updateable(name,updatePeriod){}


/******************  StupiditySensor **************************/
void StupiditySensor::_update(long unsigned now){
  _sayMyName();
  _specialStupiditySensorFunction();
}

void StupiditySensor::_specialStupiditySensorFunction(){
  Serial.println("I'm doin' special stupidity detection!    *** It's easy, there's stupidity everywhere I look!\n");
}

StupiditySensor::StupiditySensor(String name, long unsigned updatePeriod): Updateable(name,updatePeriod){}
