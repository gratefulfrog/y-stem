#ifndef SENSORS_H
#define SENSORS_H

#include "Updateable.h"

class LightSensor: public Updateable{
  protected:
    virtual void _update(long unsigned now);  // defined here!
    void _specialLightSensorFunction();

  public:
    LightSensor(String name, long unsigned updatePeriod);
};

class AcousticSensor: public Updateable{
  protected:
    virtual void _update(long unsigned now);  // defined here!
    void _specialAcousticSensorFunction();

  public:
    AcousticSensor(String name, long unsigned updatePeriod);
};

class StupiditySensor: public Updateable{
  protected:
    virtual void _update(long unsigned now);  // defined here!
    void _specialStupiditySensorFunction();

  public:
    StupiditySensor(String name, long unsigned updatePeriod);
};

#endif
