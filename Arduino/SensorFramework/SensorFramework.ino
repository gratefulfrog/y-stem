/* Here is a framework for general update-able sensors.
 *  The idea is that each sensor needs to update itself periodically but
 *  the perido is proper to the type of sensor. eg. a temp sensor could be
 *  updated once per minute, a light sensor once per second, etc.
 *  
 *  In this framework, I define an Abstract Parent Class: Updateable which
 *  is then inherited from in the child classes which carry the actual
 *  sensor implementation.
 *  
 *  We observe the elegance of the updatig in the main loop
 */

#include "Sensors.h"

const int nbSensors = 3;

// declare global pointers to the Sensor class instances
LightSensor     *pLightSensor;
AcousticSensor  *pAcousticSensor;
StupiditySensor *pStupiditySensor;

// define a vector of pointers to the abstract Updateable class !!! Note this!
Updateable      *pVec[nbSensors];

const long unsigned lightSensorPeriod     =  5000,  // every  5 seconds
                    acousticSensorPeriod  =  1000,  // every  1 second
                    stupiditySensorPeriod = 10000;  // every 10 seconds

void setup() {
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting up!");
  
  // here we create the instances of the sensor classes. I give them names just for fun!
  // and assign them to the spots in the pVec
  
  pLightSensor     = new LightSensor("Frank the Light Sensor",lightSensorPeriod);
  pVec[0] = pLightSensor;
  
  pAcousticSensor  = new AcousticSensor("Sally the Noise Sensor",acousticSensorPeriod);
  pVec[1] = pAcousticSensor;
  
  pStupiditySensor = new StupiditySensor("Bill the Stupidity Detector",stupiditySensorPeriod);
  pVec[2] = pStupiditySensor;

  Serial.println("\n*** setup finished, entering the loop..\n\n");
}

void loop() {
  static int i = 0;
  
  // Here we use the framework to update with elegance!
  pVec[i]->update(millis());
  
  i = ++i%nbSensors;
}
