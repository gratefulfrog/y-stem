#ifndef UPDATEABLE_H
#define UPDATEABLE_H

#include <Arduino.h>

class Updateable{
  protected:
    const String _name;
    const long unsigned   _updatePeriod;       // milliseconds
    long unsigned         _lastUpdateTime = 0;  // milliseconds
    
    virtual void _update(long unsigned now) = 0;  // declared like this means it must be defined in each child class
    void _sayMyName() const;

  public:
    Updateable(String name, long unsigned updatePeriod);
    void update(unsigned long now);
};

#endif
