#!/usr/bin/python3
# this tells the operating system whichy python interprettuer
#to to use to execute this code if called from the command line eg.  $
# ./simpleGui.py (first make the program executable with $ chmod a+x simpleGui.py


# first we need to import the python gui library
from tkinter import *
# let's import everything so we don't have to prefix each call

master = Tk()                     # declare the master window as a global  Tk window

# first example: the simplest of gui
# 0
def simplest():    
    master.title('Hello Sensei')      # give it a title    

# second example: simplest but nicer
# 1
def simplestNicer():
    simplest()
    master.title('Try ctrl-q')      # give it a title    
    master.bind('<Control-q>', quit)  # make ctrl-q the quit command

# second example: simplestNicer but even nicer, we need a message box
# 2
from tkinter import messagebox
def simplestNicerStill():
    simplestNicer()
    master.geometry('800x600')  # add the window size
    master.bind('<Control-q>', doQuitMessage)  # make ctrl-q call the function 'doQuitMessage'

def doQuitMessage(*unused):
    # called when Ctrol-q is typed, show a message and quit
    messagebox.showinfo(title='Bye!', message= 'See you later, Sensei')
    quit()

# how about displaying a label!
# 3
def sizeAndLabel():
    simplestNicerStill()
    # define a label
    aSimpleLabel = Label(master,
                         text= 'Please resize me!',
                         bg = 'yellow',      # background color
                         relief=RAISED)
                         
    aSimpleLabel.grid(sticky = 'NESW')   # put it in the master window grid at the
    # default position which is in this case row 0 column 0 and make it stick to
    # all sides
    # the next 2 lines make the label expand to fill all available space and stay
    # that way even if the windo is resized!
    master.grid_rowconfigure(0, weight=1) #  set row zero to be the full weight of
    #  the horizontal space
    master.grid_columnconfigure(0, weight=1) # set column zeo to be the ful lweight
    # of the vertical space

# a label who's text change when we click
# 4
def activeLabel():
    simplestNicerStill()
    # define a label, but instead of assigning it text, assign it a 'textvariable'
    activeLabel = Label( master, textvariable=labelText, relief=RAISED)
    # bind the left mouse to the function 'onClick'
    master.bind('<Button-1>', onClick)
    # configure label to fill space and center as before
    activeLabel.grid(sticky = 'NESW')   # put it in the master window grid at the
    # default position which is in this case row 0 column 0, and make ti stick to
    # all sides
    master.grid_rowconfigure(0, weight=1) #  set row zero to be the full weight of
    #  the horizontal space
    master.grid_columnconfigure(0, weight=1) # set column zeo to be the ful lweight
    # of the vertical space    activelabel.grid()
    onClick()

# these following items are used to make the text of the label active
# first define the labelText to be a Tkinter 'StringVar' instance
labelText = StringVar()
# now make a list of text to display in the label
labelTextVector = ['Click Me!',
                   'Hi Ulrike',
                   'Hi Yannick']
# define a variable containing the index of the xt to display
labelTextIndex = 0  # the index of the current text

# this is called on mouse left click
def onClick(*unused):
    global labelTextIndex  # tell python that we are using the global variable, not
    # a local one
    # set the textvariable to contant the current element in the list of text to display
    labelText.set(labelTextVector[labelTextIndex])
    # increment the index, but restart at zero if we reach the limit
    labelTextIndex =  (labelTextIndex + 1 ) % len(labelTextVector)


######################  Command line argument processing
########################################################

def runFromCommandLine():
    # I propose to read command line arguments as integers and call the code depending
    # on the arg. for example ./simpleGui.py 0 calls element zero in the vector of gui examples
    # see at the very end of the file for the calling
    guiExampleVec = [simplest,             # 0
                     simplestNicer,        # 1
                     simplestNicerStill,   # 2
                     sizeAndLabel,         # 3
                     activeLabel]          # 4
    # now we need to get the command line arguments
    # first import argv from the sys library
    from sys import argv
    # now get the vector of command line args which is always of the form
    # command arg0 arg1 arg2...
    # if the length is < 2 then there was no arg, provide help and exit
    if len(argv) != 2:
        print('Please provide numerical argument less than ' + str(len(guiExampleVec)) + ' to select the gui to show')
    else:
        # get the argument, convert to an int and call the corresponding function
        # in the vector
        guiExampleVec[int(argv[1])]()   # this sets up our gui depending on the arguemnt
        master.mainloop()                 # start the infinite event loop


    
# the following 'if' is TRUE if the file is called at the command line,
# if not nothing happens so we can also import this file into the python
# interpretter and it will work the same
if __name__ == '__main__':
    runFromCommandLine()
