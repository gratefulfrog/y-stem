#!/usr/bin/python3


class CircularList:
    """ Circular List implementation.
    By defalut overwriting values
    if too many elements are added.
    but if overwrite is false, raises exception when an 
    attempt to add too many elts is made
    """
    
    def __init__(self, size=256, overwrite = True):
        if size <= 0:
            raise ValueError('size must be greater than zero')
        self.size      = size
        self.overwrite = overwrite
        self.getIndex  = 0
        self.putIndex  = 0
        self.nbElts    = 0
        self.eltVec    = [None for x in range(self.size)]
        
    def put(self,item):
        if self.nbElts == self.size:
            if self.overwrite:
                self.getIndex = (1+self.getIndex)%self.size
            else:
                raise OverflowError
        else:
            self.nbElts +=1
        self.eltVec[self.putIndex] = item
        self.putIndex = (1+self.putIndex)%self.size

    def get(self):
        if self.nbElts:
            res = self.eltVec[self.getIndex]
            self.getIndex = (1+self.getIndex)%self.size
            self.nbElts -=1
            return res
        else:
            raise StopIteration

    def __str__(self):
        return (  'Size: ' + str(self.size)      + \
               '\nNb Elts: ' + str(self.nbElts) + \
               '\nElts: ' + str(self.eltVec))

    def __iter__(self):
        return self

    def __next__(self):
        return self.get()
    

if __name__ == '__main__' :
    from time import sleep
    c = CircularList(10) #, overwrite=False)

    for i in range(50):
        c.put(i)
        print(c)

    i =iter(c)
    while True:
        try:
            print(next(i))
        except StopIteration:
            print('Done')
            break
    sleep(5)
    j = 0
    while j< 1000:
        c.put(j)
        j+=1
        try:
            print(next(i))
        except StopIteration:
            print('Done')
            break            
    sleep(5)
    
    cc = CircularList(10,overwrite=False)
    for i in range(50):
        cc.put(i)
        print(c)
    i =iter(cc)
    while True:
        try:
            print(next(i))
        except StopIteration:
            print('Done')
            break

