#!/usr/bin/python3

import RPi.GPIO as GPIO
from time import sleep

def doit():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW)
    while True:
        GPIO.output(8, not GPIO.input(8))
        sleep (1)

if __name__ == '__main__':
    doit()
    
