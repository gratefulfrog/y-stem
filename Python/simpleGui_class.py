#!/usr/bin/python3
# this tells the operating system whichy python interprettuer
#to to use to execute this code if called from the command line eg.  $
# ./simpleGui.py (first make the program executable with $ chmod a+x simpleGui.py


# first we need to import the python gui library, let's shorten its name to 'tk'
import tkinter as tk

# we will need the messagebox
from tkinter import messagebox

class activeLabelGui(tk.Frame):
    def __init__(self,master= None):
        tk.Frame.__init__(self,master)
        self.master.title('Hello Sensei - Try ctrl-q')       # give it a title    
        self.master.geometry('800x600')  # add the window size

        # now make a list of text to display in the label
        self.labelText = tk.StringVar()
        self.labelTextVector = ['Click Me!',
                                'Hi Ulrike',
                                'Hi Yannick']
        self.labelTextIndex = 0  # the index of the current text
        self.activeLabel = tk.Label(self.master,
                                    textvariable=self.labelText,
                                    relief=tk.RAISED)

        # bind the left mouse to the function 'onClick' and ctrl q to doQuitMessage
        self.master.bind('<Control-q>', self.doQuitMessage)  # make ctrl-q call the function 'doQuitMessage'
        self.master.bind('<Button-1>', self.onClick)

        # default position which is in this case row 0 column 0, and make it stick to
        # all sides
        self.activeLabel.grid(sticky = 'NESW')   
        
        #  set row zero to be the full weight of  the horizontal space
        self.master.grid_rowconfigure(0, weight=1) 

        # set column zeo to be the full weight of the vertical space
        self.master.grid_columnconfigure(0, weight=1)

        # call to set up the label
        self.onClick()
        
    def doQuitMessage(self,*unused):
        # called when Ctrol-q is typed, show a message and quit
        messagebox.showinfo(title='Bye!', message= 'See you later, Sensei')
        quit()

    def onClick(self,*unused):
        # set the textvariable to contain the current element in the list of text to display
        self.labelText.set(self.labelTextVector[self.labelTextIndex])
        # increment the index, but restart at zero if we reach the limit
        self.labelTextIndex =  (self.labelTextIndex + 1 ) % len(self.labelTextVector)


######################  Command line argument processing
########################################################

def runFromCommandLine():
    ourGui = activeLabelGui()
    ourGui.mainloop()   # start the tkinter event loop

if __name__ == '__main__':
    runFromCommandLine()
